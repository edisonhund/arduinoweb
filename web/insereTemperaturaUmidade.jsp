<%-- 
    Document   : index
    Created on : 17-Jul-2014, 16:09:56
    Author     : Edison
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    //Instacia a classe responsavel pelo banco
    Principal.DAO dao = new Principal.DAO();

    //Armazena a Data de hoje em String
    Date data = new Date(System.currentTimeMillis());
    //SimpleDateFormat formatarDate = new SimpleDateFormat(“yyyy-MM-dd”);
    SimpleDateFormat formatarDate = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    String strData = formatarDate.format(data);

    ResultSet resultSet;
    
    //abre o banco e envia para o driver para a conexão
    dao.open();

    try {  
        //Query para Inserção no Banco de Dados e os parametros recebidos via GET
        //String sql = "Insert into arduino.TabelaSensor(hora, temperatura, umidade) values('" + strData + "'," + request.getParameter("temperatura") + ",'" + request.getParameter("umidade") + "')";
        String sql = "INSERT INTO arduino.tabelasensor(hora, temperatura, umidade)"
                        + " VALUES('" + strData + "','33','11')";
            //executa a query, note que uso o metodo executeUpdate() que pode ser usado para Inserir, Update ou Excluir
        //retorna um inteiro 1 caso a transição seja realizada com sucesso ou 0 caso haja algum erro

        int reg = dao.executeUpdate(sql);

        if (reg != 0) {                       
            sql = "SELECT * FROM arduino.tabelasensor"
                    + " WHERE hora = (SELECT MAX(hora) FROM arduino.tabelasensor)";

            resultSet = dao.executeQuery(sql);

            resultSet.first();

 %>
<%--
        <table summary="Table of last info registered" border="1" cellspacing="3" cellpadding="3">
            <caption> Data included! </caption>
                <tr>
                    <th id="colTime"> Time </th>
                    <th id="colTime"> Temp ºC </th>
                    <th id="colTime"> Humidity </th>
                </tr>
                <tr>
                    <td ALIGN=MIDDLE><%out.print(resultSet.getString(1));%></td>
                    <td ALIGN=MIDDLE><%out.print(resultSet.getString(2));%></td>
                    <td ALIGN=MIDDLE><%out.print(resultSet.getString(3));%></td>
                </tr>
        </table>

--%>
 <%                                 
          //fecha a conexão
            dao.close();
        } else {
            out.println(sql);

            out.println("Include data error!   ");
            //fecha a conexão
            dao.close();
        }
                       
    } catch (Exception ex) {
        //out.print(ex);
    }
%>

<%--
    <br>
    <input type="submit" value="Back" name="backButton" onclick="location.href = 'index.jsp'"/>
--%>