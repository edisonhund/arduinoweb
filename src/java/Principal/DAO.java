/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

/**
 *
 * @author Edison Autoident: alt+shift+f
 */
import java.sql.*;

public class DAO {

    private Connection connection;
    private Statement statement;
    private String username;
    private String password;
    private String url;
    private String driver;

    public void setUsername(String username) {
        this.username = username;
       // System.out.println("username: " + username);
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(String password) {
        this.password = password;
      //  System.out.println("password: " + password);
    }

    public String getPassword() {
        return this.password;
    }

    public void setUrl(String url) {
        this.url = url;
      //  System.out.println("url: " + url);
    }

    public String getUrl() {
        return this.url;
    }

    public void setDriver(String driver) {
        this.driver = driver;
       // System.out.println("driver: " + driver);
    }

    public String getDriver() {
        return this.driver;
    }

    public Connection getConnection() {
        return this.connection;
    }

    public Statement getStatement() {
        return this.statement;
    }

    public void DataBase() {
        try {

            setDriver("com.mysql.jdbc.Driver");
            setUrl("jdbc:mysql://localhost/");
            setUsername("root");
            setPassword("");

            Class.forName(getDriver());

        } catch (ClassNotFoundException error) {
            System.out.println(error);
        }

    }

    public void open() {
        try {
            DataBase();
            connection = DriverManager.getConnection(getUrl(), getUsername(), getPassword());
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

        } catch (SQLException error) {
            System.out.println(error);
        }
    }

    
    public void close() {
        try {
            connection.close();
        } catch (SQLException error) {
            System.out.println(error);
        }
    }

    public ResultSet executeQuery(String sql) {
        ResultSet rs = null;
        try {
          //  System.out.println("sql executeQuery: " + sql);
            rs = statement.executeQuery(sql);
        } catch (SQLException error) {
            System.out.println(error);
          //  System.out.println("error executeQuery : ");
        }
        return rs;
    }

    public int executeUpdate(String sql) {
        int result = 0;
        try {
           // System.out.println("sql2  -  "+sql+"  -  ");
            result = statement.executeUpdate(sql);
        } catch (SQLException error) {
            System.out.println(error);
           // System.out.println("error executeUpdate:");
        }
        return result;
    }

}
